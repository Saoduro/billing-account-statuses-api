var response = JSON.parse(context.getVariable("response.content"));
if (typeof response != 'undefined') {
    // Add correlationId from request to response
    response.correlationId = context.getVariable("correlationId");
    context.setVariable("response.content", JSON.stringify(response));
}
    
